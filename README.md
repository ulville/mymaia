# myMaia

Terminal colorscheme that fits the default themes used in Manjaro.

![](Ekran_Görüntüsü_-_2020-05-01_23-06-35.png)

I created this terminal colorscheme with the inspiration of Maia/Matcha-sea/Breath themes used in Manjaro by default, so it fits well to folks who'd like a consistent Manjaro theme while using a terminal.

## Usage

I created this colorscheme in gnome-terminal but for the sake of universalization I turned it into a .Xresources file. So you can use the website [terminal.sexy](http://terminal.sexy) and import the file and export it as almost any format like gnome-terminal, konsole, xfce4-terminal, termite, terminator, alacritty, st, etc. You can even create a bash script to apply the colorscheme in linux console (TTY) (the one you get with ctrl+alt+Fx keys). 

xterm or rxvt users can use the file as it is. Just copy the content and paste it at the end of your .Xresources file. Then run `xrdb ~/.Xresources` command.
For others; use [terminal.sexy](http://terminal.sexy) website. It either creates a bash script to install the scheme or creates a config file for your spesific terminal emulator.

